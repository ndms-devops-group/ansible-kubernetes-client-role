# -*- mode: ruby -*-
# vi: set ft=ruby :

IMAGE_NAME="ndms-centos7-x86_64-virtualbox"

$git_package="git"

Vagrant.configure("2") do |config|
	config.ssh.insert_key = false # disable key based SSH because it looks like it messes up cluster comms
	config.vm.define "master" do |master|
        master.vm.box = IMAGE_NAME
		master.vm.hostname = 'master'
		master.ssh.username = 'vagrant'
		master.ssh.password = 'vagrant'
		master.vm.network "private_network", ip: "192.168.60.60", virtualbox__intnet: true
		master.vm.network "forwarded_port", guest: 22, host: 2000, id: "sshPort"
		master.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=777"]
		master.vm.provider "virtualbox" do |vm|
			vm.memory = "1024"
			vm.cpus = "1"
		end
		update_ansible(master)
		add_docker_group_to_user(master)
		install_git(master)
		install_required_collections(master)
	end
end

def forward_port_range(node, min_port, max_port) 
	for p in min_port..max_port
		node.vm.network "forwarded_port", guest: p, host: p, id: "portf-#{p}"
	end
end

def update_ansible(node)
	node.vm.provision "shell", inline: <<-SHELL
	exec /vagrant/vagrant_setup/scripts/update_ansible.sh
	SHELL
end

def add_docker_group_to_user(node)
	node.vm.provision "shell", :args => [node.ssh.username], inline: <<-SHELL
	echo 'Adding docker group to current user: '$1
	sudo usermod -aG docker $1
	echo 'finished install for user: '$1
	SHELL
end

def install_required_roles(docker)
	docker.vm.provision "shell", inline: <<-SHELL
		ansible-galaxy install -cr /vagrant/requirements_local.yml --roles-path=/vagrant/roles
		sudo chown -R vagrant:vagrant /vagrant/roles
	SHELL
end

def install_required_collections(node)
	node.vm.provision "shell", inline: <<-SHELL
		ansible-galaxy collection install -r /vagrant/requirements_local.yml
	SHELL
end

$install_git = <<-SCRIPT
	GIT_PKG=$1

	remove_previous_install() {
    	echo "Removing previous installation and unnecessary packages..."
    	sudo yum -y remove $GIT_PKG
	}

	install_git() {
		echo "Installing the following package: $GIT_PKG"
    	sudo yum -y install $GIT_PKG
	}
	remove_previous_install
	install_git
SCRIPT

def install_git(docker)
	docker.vm.provision "shell", inline: $install_git, :args => [$git_package]
end
